<?php 

    // пункт №1

    $continents = [
        'Eurasia' => 
            [
            'ursus arctos', 'canis lupus', 'bison'
        ],            
        'Africa' => 
            [
            'panthera pardus', 'gazella gazella', 'galago'
        ],
        'North_America' => 
            [
            'mustela erminea', 'crocodilus acutus', 'antilocapra americana'
        ],
        'South_America' => 
            [
            'lama guananico', 'choloepus didactylus', 'felis pardalis'
        ],
        'Australia' => 
            [
            'phascoarctos cinereus', 'macropus major', 'dendrolagus'
        ],
        'Antarctica' => 
            [
            'pygoscelis antarctica', 'phoca vitulina', 'balaenoptera musculus'
        ]   
    ];

    echo "<pre>";
    print_r($continents);


// пункт №2

    $two_words = [];
    $first_word = [];
    $second_word = []; 
    foreach ($continents as $continent => $animals) {
        foreach ($animals as $selected_animals) {
            if (substr_count($selected_animals, ' ') == 1) {
            $parts = explode(" ", $selected_animals);
            $first_word[$continent][] = $parts[0];
            $second_word[] = $parts[1]; 
            $two_words[$continent][] = $selected_animals;
            }
        }         
    }

    print_r($two_words);

    // пункт №3
       
    print_r($first_word);
    print_r($second_word);

    shuffle($second_word);
    print_r($second_word);

    $finish_name = [];
    $i = 0;
    foreach ($first_word as $continent => $animals) {
        foreach ($animals as $value) {
            $finish_name[$continent][] = $value. " " .$second_word[$i++];
        }
    }

    print_r($finish_name);

     //дополнительное задание 

    foreach ($finish_name as $continent => $animals) {
    echo "<h2>$continent</h2>";
    echo (implode(", ", $animals));
    }

?>